# Solution au devoir 2

## Auteur

Olivier Petit: PETO19049107

Python 3.6


## Solution à la question 1

Cette solution est basé sur un projet github.

Source: https://github.com/buckinha/DiamondSquare

Certain partie du code de la solution 1 sont des prises de cette solution et la logique derrière
l'implémention aussi.

Le code comporte des commentaires pour expliquer comment le code fonctionne et se que les sections et méthodes accomplissent. 

Pour le numéro 1.1, il n'y a que q1.py

Pour éxécuter le code, il suffit de saisir par exemple: 

python q1.py heightmap 8 heightmap.png.

Pour faire fonctionner le tout, numpy est essentiel et PIL Pillow, ainsi que python 3.6.

Pour la question 1.2: il faut lancer la commande : 

python q1.py normal heightmap.png normal_map.png

Par contre: 

J'ai réussi à loader mon image et la mettre en data dans une matrice de float.
Je tentais de metre une coordonnée RGB à chaque place dans la matrice.
Ensuite, je voulais générer ma normal_map avec c'est coordonnées qui m'auraient données la couleur.


Je pense que mon raisonnement n'est pas bon car, je perd mes valeurs de ma carte originale.
Ces valeurs sont justement supposées me donner la difference de couleur pour mon RGB
J'ai trouvé une façon de faire, mais elle ne répondait pas au exigeance de la question et demandait 2 png pour être éxécuté,
sinon elle donnait une carte des normales uniforme, donc complètement mauve.


## Solution à la question 2

Même chose pour la question 2

## Dépendances

numpy
PIL Pillow
Python 3.6
SciPy


## Références

Pour la question 1.1:

Source: 

https://github.com/buckinha/DiamondSquare

Stackoverflow

Pour la question 1.2:

Source: 

Stackoverflow

https://blenderartists.org/t/ripples-waves-normalmaps-and-textures-with-numpy-scipy-and-matplotlib/454787/7

Pour la question 2

J'ai regarder le comportement de certains exemples sur ShaderToy.

Stackoverflow

## État du devoir

1.1: Fini

1.2: Non fini, la fonction est en commentaire car elle cause une boucle infini

2.1: Fini

2.2: Fini
