import random
import sys
import numpy as np
import scipy.ndimage as sp

from PIL import Image


def diamond_square(taille, pronfondeurMin, pronfondeurMax):

    taille_Carte = [2 ** taille + 1,2 ** taille + 1]
    surface = 0.50
    iterations = taille
    Taille_matrice = taille_Carte[0]
  
    #Faire une matrice de la bonne taille avec des 0.0 à chaque position
    matrice = np.zeros((Taille_matrice,Taille_matrice), dtype='float')
    #On remplis la matrice avec la valeur -1 qui indiquera que la valeur doit être modifié
    matrice = matrice - 1.0
    
    #Selon l'exemple, je dois remplir les coins de la matrice avec des valeurs aléatoire
    matrice[0,0] = random.uniform(0, 1)
    matrice[Taille_matrice-1,0] = random.uniform(0, 1)
    matrice[0, Taille_matrice-1] = random.uniform(0, 1)
    matrice[Taille_matrice-1, Taille_matrice-1] = random.uniform(0, 1)

    #Algorithme
    for i in range(iterations):
        r = surface**i
        step = (Taille_matrice-1) / 2**(i)
        
        diamond(matrice, step, r)        
        square(matrice, step, r)      
  
    # Dans la solution de https://github.com/buckinha/DiamondSquare, nous devons redimensionner la matrice,
    # Nous obtenons une matrice complètement noire
    matrice = pronfondeurMin + (matrice * (pronfondeurMax - pronfondeurMin))
    
    return matrice  

def diamond(matrice, step, surface):  
    
    # ici on divise le step par 2 pour optenir le centre, donc le diamond
    milieu = step/2
    
    x = range(int(milieu), matrice.shape[0], int(step))
    # l'utilisation de [:] est nécessaire pour faire une copie exacte du tuple de x
    y = x[:]
    
    for i in x:
        for j in y:
            # si la valeur dans la matrice est hors des bornes, on déplace le diamond
            if matrice[i,j] == -1.0: 
                ul = matrice[i-int(milieu), j-int(milieu)]
                ur = matrice[i-int(milieu), j+int(milieu)]
                ll = matrice[i+int(milieu), j-int(milieu)]
                lr = matrice[i+int(milieu), j+int(milieu)]
                
                matrice[i,j] = (surface*(random.uniform(0,1)) + (1.0-surface)*((ul + ur + ll + lr)/4.0))

def square(matrice, step, surface):
   
    milieu = step/2
    
    #verticale
    x_vert = range(int(milieu), matrice.shape[0],int(step))
    y_vert = range(0,matrice.shape[1],int(step))

    #horizontale
    x_horiz = range(0, matrice.shape[0],int(step))
    y_horiz = range(int(milieu), matrice.shape[1],int(step))

    # on passe sur les axes pour place les squares
    for i in x_horiz:
        for j in y_horiz:
            matrice[i,j] = deplacement_milieu(matrice, i, j, milieu, surface)

    for i in x_vert:
        for j in y_vert:
            matrice[i,j] = deplacement_milieu(matrice, i, j, milieu, surface)


def deplacement_milieu(matrice, i, j, milieu, surface):
    
    # 4 division, car 4 possibilité, haut, bas, gauche ,droite
    nbrDivision = 4
    
    total = 0.0

    if i - milieu >= 0:
        total += matrice[i-int(milieu), j]
    else:
        nbrDivision -= 1
   
    if i + milieu < matrice.shape[0]:
        total += matrice[i+int(milieu), j]
    else:
        nbrDivision -= 1
   
    if j - milieu >= 0:
        total += matrice[i, j-int(milieu)]
    else:
        nbrDivision -= 1
    
    if j + milieu < matrice.shape[0]:
        total += matrice[i, j+int(milieu)]
    else:
        nbrDivision -= 1

    return (surface*(random.uniform(0,1)) + (1.0-surface)*(total / nbrDivision))

def normalMap(img):
    """
    Je tentais de metre une coordonnées RGB avec les fonctions données dans l'exemple de stackoverflow
    Ensuite, je voulais générer ma normal_map avec c'est coordonnées
    Je pense que mon raisonnement n'est pas bon car je perd mes valeurs de ma carte originales.
    C'est valeur sont justement supposer me donner la difference de couleur pour mon RGB
    """
    #(i, j), (i, j + 1), (i + 1, j), and (i + 1, j + 1)  
    # je parcour ma matrice
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            initI = i
            initJ = j
            r = np.array([float(i), float(j + 1)]) 
            #print(r)            
            g = np.array([float(i + 1),float(j)])
            #print(g)            
            b = np.array([float(i + 1), float(j + 1)])
            #print(b)            
            pixelRGB  = np.array([r,g,b]).shape 
            np.insert(img, [initI,initJ], pixelRGB) 
           
            #   img[i,j] = np.array([pixelRGB])
            i = initI +1
            j = initJ +1 
            
    print(img)
    
    
def main():
     
    if sys.argv[1] == "heightmap" :
        # solution basée sur https://github.com/buckinha/DiamondSquare
        surface = 0.50       
        carte = diamond_square(int(sys.argv[2]),0.0,100.0)    
        img = Image.fromarray(carte).convert("RGB")
        img.save(sys.argv[3],"PNG")
        
    elif sys.argv[1] == "normal":
    
        #call normal_map
        #ouverture de l'image
        img = Image.open('heightmap.png')
        # le load les données de l'image dans une matrice
        np_img = np.array(img.getdata()).astype(float) 
        #Je commente l'appel à la fonction, car elle fait une boucle infini...
        #normalMapImg = normalMap(np_img)
        
                   

if __name__ == "__main__":
    
    main()
