##2.1

#define PI 3.14159265
#define A 20.0

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    
    vec2 dist = (center - fragCoord - 0.5 );
    // length done la longeur du vector
    // donc ici jai la longueur du vector centre et fragCoor - 0.5
    float r = length(dist);    
    
    // retourne l'arc tangente  / 2*pi
    float theta = atan(dist.y , dist.x) / (2.0 * PI) + iTime;    
    r = (r - A  * theta);
    
    float c = (r / A - (floor(r / A)));
    
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)));

}


#2.2

#define PI 3.14159265
#define A 20.0

float add_New_Line(float color, float shape)
{
    return color + shape;
}


void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec3 color = vec3(0.0);

    
    vec2 dist = (center - fragCoord - 0.5 );
    // length done la longeur du vector
    // donc ici jai la longueur du vector centre et fragCoor - 0.5
    float r = length(dist);    
    
    // retourne l'arc tangente  / 2*pi
    float theta = atan(dist.y , dist.x) / (2.0 * PI) + iTime;    
    r = (r - A  * theta);
    
    float c = (r / A - (floor(r / A)));   
     
    
   
    color.r = add_New_Line(smoothstep(0.2, 0.0, abs(c - 0.25)), abs(c - 0.25));
    color.g = add_New_Line(smoothstep(0.0,0.2, abs(c - 0.25)), abs(c - 0.25));
    color.b = add_New_Line(smoothstep(0.2, 0.2,abs(c - 0.25)), abs(c - 0.25));
       
	fragColor = vec4(color,1.0);
}
    
    
        

    
   